<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\Services;


class TurboSpeed implements BoosterProvider
{
    public function addBooster(): int
    {
        return 60;
    }
}