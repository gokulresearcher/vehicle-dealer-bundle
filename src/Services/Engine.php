<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\Services;

class Engine
{
    private $unit;

    private $turboSpeed;

    public function __construct(BoosterProvider $turboSpeed, string $unit)
    {
        $this->unit = $unit;
        $this->turboSpeed = $turboSpeed;
    }

    public function getSpeed(): string
    {
        return sprintf('10 %s', $this->unit);
    }

    public function applyTurboSpeed(int $currentSpeed): string
    {
        $currentSpeed += $this->turboSpeed->addBooster();
        return sprintf('%s %s', $currentSpeed, $this->unit);
    }
}