<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\Services;


interface BoosterProvider
{
    public function addBooster(): int;
}