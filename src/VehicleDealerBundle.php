<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle;


use Gocool\VehicleDealerBundle\DependencyInjection\GocoolVehicleDealerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class VehicleDealerBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new GocoolVehicleDealerExtension();
        }
        return $this->extension;
    }
}
