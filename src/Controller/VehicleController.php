<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\Controller;

use Gocool\VehicleDealerBundle\Services\Engine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VehicleController extends AbstractController
{
    private $engine;

    public function __construct(Engine $engine)
    {
        $this->engine = $engine;
    }

    /**
     * @Route(path="/vd-speed", name="show_message", methods={"GET"})
     */
    public function displaySpeed(): Response
    {
        return new Response('Speed is : '.$this->engine->getSpeed());
    }
}
