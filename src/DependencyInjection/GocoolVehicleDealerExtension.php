<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class GocoolVehicleDealerExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);


        if (null !== $config['engine']['booster_provider']) {
            $container->setAlias(
                'gocool_vehicle_dealer.booster_provider',
                $config['engine']['booster_provider']
            );
        }

        $definition = $container->getDefinition('Gocool\VehicleDealerBundle\Services\Engine');
        $definition->setArgument(1, $config['engine']['unit']);
    }

    public function getAlias()
    {
        /**
         * If you dont want alias then in "config/packages/gocool_vehicle_dealer.yaml"
         * use root as gocool_vehicle_dealer which is default naming standards
         */
        return 'gocool_vd';
    }
}