<?php

declare(strict_types=1);

namespace Gocool\VehicleDealerBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('gocool_vehicle_dealer');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('engine')
                    ->children()
                        ->scalarNode('unit')->defaultValue('KM/H')->end()
                        ->scalarNode('booster_provider')->defaultNull()->end()
                    ->end()
                ->end() //engine
            ->end()
        ;

        return $treeBuilder;
    }
}